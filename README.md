# INTEGRACION COVID API

## Ejecutar la aplicacion con Docker localmente
Para iniciar la aplicación se debe utilizar el siguiente script.

```bash
start_local_containers.sh
```

Para detener la aplicación se debe ejecutar el script:

```bash
stop_local_containers.sh
```

Para revertir la aplicación al momento cero se debe utilizar el script:

```bash
reset_local_containers.sh
```

Para eliminar el sistema completamente se debe utilizar el script:

```bash
delete_local_containers.sh
```

El archivo docker-compose-dev.yml tiene todos los parámetros de conexión a la vista.
Normalmente este archivo tendría que estar ignorado en el .gitignore, y los desarrolladores lo pueden tener localmente en su pc, con datos "DEV".
Se mantuvo esto para poder probar los containers de manera rápida (al corregir).

## Build/Release

El build para "producción" utiliza el docker-compose.yml. La parametrizacion de las configuraciones de cada aplicación se hace desde el Azure Web App.

Un push en master del repositorio activa un pipeline de CI en Azure DevOps, el cual compila y publica las imagenes en Docker Hub.
<br>
Migramos de Azure Container Registry a Docker Hub para minimizar los costos.

Además tenemos tres release pipelines que detectan cuando se cambian estas imágenes en el registro y las publican a su correspondiente Azure Web App.

Desde Azure DevOps se configuraron las conexiones contra GitLab para obtener el código, contra DockerHub para publicar las imágenes y contra Azure para publicarlas en los Azure Web App.

A su vez, la DB tiene en su firewall habilitadas únicamente las IP del WebApp de Covid API.
<br>
Migramos de MySQL database a SQL Server para minimizar los costos.

## Endpoints

**SQL Server**: utnbicifrba.database.windows.net
<br>
**SQL database**: tacsgrupo1

**Covid API**: tacsgrupo1covidapi.azurewebsites.net

**Front End**: tacsgrupo1covidfrontend.azurewebsites.net

**Telegram Bot**: tacsgrupo1covidtelegrambot.azurewebsites.net
