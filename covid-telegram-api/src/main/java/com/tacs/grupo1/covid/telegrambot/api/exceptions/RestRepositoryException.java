package com.tacs.grupo1.covid.telegrambot.api.exceptions;

public class RestRepositoryException extends RuntimeException {
    public RestRepositoryException() {
        super("Repository error.");
    }

    public RestRepositoryException(String message) {
        super(message);
    }
}
