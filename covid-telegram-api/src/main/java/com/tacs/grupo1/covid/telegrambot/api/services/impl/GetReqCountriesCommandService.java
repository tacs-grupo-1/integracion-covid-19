package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.dto.Countries;
import com.tacs.grupo1.covid.telegrambot.api.dto.Country;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.CountryRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Service
public class GetReqCountriesCommandService implements CommandService {

    private final CountryRepository countryRepository;

    @Autowired
    public GetReqCountriesCommandService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {
        if (update.hasCallbackQuery()) {

            return new EditMessageText()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText("Consultar ultimos valores del pais")
                    .setParseMode(ParseMode.MARKDOWN)
                    .setReplyMarkup(generateKeyboardFromCountries(
                            countryRepository.getCountries(1),
                            "/"+Command.GETCOUNTRYSTATS.toString().toLowerCase(),
                            "/"+ Command.GETREQOPTION.toString().toLowerCase()));

/*                                .setReplyMarkup(generateKeyboardFromCountries(
                    countryRepository.getCountries(1).getCountries(),
                    Command.GETCOUNTRYSTATS.toString().toLowerCase(),
                    "/"+ Command.GETREQOPTION.toString().toLowerCase()));
*/
        }
        else if (update.hasMessage()){
            return new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText("Comando no aceptado por mensaje!");
        }

        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }

    private InlineKeyboardMarkup generateKeyboardFromCountries(List<Country> countries, String command,String backAction){
        InlineKeyboardBuilder keyboard = InlineKeyboardBuilder.create();

        boolean isEndRow=false;

        for (Country e: countries) {
            if(!isEndRow){
                keyboard
                        .row()
                        .button(e.getName() +"("+e.getIsoCountryCode()+")",
                                command + " " + e.getId());
            }
            else{
                keyboard
                        .button(e.getName() +"("+e.getIsoCountryCode()+")",
                                command + " " + e.getId())
                        .endRow();
            }
            isEndRow=!isEndRow;
        }

        if (isEndRow)
            keyboard.endRow();

        keyboard.row().button("<< Back",backAction).endRow();

        return keyboard.buildInLineKeyboard();
    }

}
