package com.tacs.grupo1.covid.telegrambot.api.factories.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.factories.CommandServiceFactory;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.services.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DefaultCommandServiceFactory implements CommandServiceFactory {

    private final ApplicationContext context;

    @Autowired
    public DefaultCommandServiceFactory(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public CommandService create(Command command) {
        if (mapOfClassServices().containsKey(command)) {
            return context.getBean(mapOfClassServices().get(command));
        }
        return context.getBean(mapOfClassServices().get(Command.HELP));
    }

    private static Map<Command, Class<? extends CommandService>> mapOfClassServices() {
        return Map.ofEntries(
                Map.entry(Command.START, StartCommandService.class),
                Map.entry(Command.SIGNUP, SignupCommandService.class),
                Map.entry(Command.SIGNOUT, SignoutCommandService.class),
                Map.entry(Command.MENU, MenuCommandService.class),
                Map.entry(Command.GETREQOPTION, GetReqOptionCommandService.class),
                Map.entry(Command.GETOPLISTS, GetOpListsCommandService.class),
                Map.entry(Command.GETREQCOUNTRIES, GetReqCountriesCommandService.class),
                Map.entry(Command.GETCOUNTRYSTATS, GetCountryStatsCommandService.class),
                Map.entry(Command.GETREQLISTS, GetReqListsCommandService.class),
                Map.entry(Command.GETCOUNTRIESSTATSINLIST, GetCountriesStatsInListCommandService.class),
                Map.entry(Command.GETOPERATIONSINLIST, GetOperationsInListCommandService.class),
                Map.entry(Command.GETCOUNTRIESINLIST, GetCountriesInListCommandService.class),
                Map.entry(Command.ADDCOUNTRIESINLIST, AddCountriesInListCommandService.class),
                Map.entry(Command.ADDCOUNTRIESINLISTACT, AddCountriesInListActCommandService.class),
                Map.entry(Command.HELP, HelpCommandService.class),
                Map.entry(Command.TABLESMENU, TablesMenuCommandService.class),
                Map.entry(Command.CONFIRMADOS, ConfirmadosCommandService.class),
                Map.entry(Command.MUERTOS, MuertosCommandService.class),
                Map.entry(Command.RECUPERADOS, RecuperadosCommandService.class),
                Map.entry(Command.GENSTATISTICSTABLE, GenStatisticsTableCommandService.class),
                Map.entry(Command.GETCOUNTRIESINDEXFORCONSULT, GetCountriesIndexForConsultCommandService.class),
                Map.entry(Command.DOCOUNTRIESSEARCHFORCONSULT, DoCountriesSearchForConsultCommandService.class),
                Map.entry(Command.GETCOUNTRIESINDEXFORLISTOP, GetCountriesIndexForListOpCommandService.class),
                Map.entry(Command.DOCOUNTRIESSEARCHFORLISTOP, DoCountriesSearchForListOpCommandService.class),
                Map.entry(Command.GETIMG, GetImgCommandService.class)
        );
    }
}
