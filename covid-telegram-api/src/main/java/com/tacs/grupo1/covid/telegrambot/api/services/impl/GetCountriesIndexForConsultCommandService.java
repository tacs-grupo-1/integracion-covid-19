package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class GetCountriesIndexForConsultCommandService implements CommandService {
    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {

        if (update.hasCallbackQuery()) {

            return new EditMessageText()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText("Índice de paises:")
                    .setParseMode(ParseMode.MARKDOWN)
                    .setReplyMarkup(InlineKeyboardBuilder.create()
                            .row()
                            .button("A", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " A" + " 0")
                            .button("B", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " B" + " 0")
                            .button("C", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " C" + " 0")
                            .button("D", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " D" + " 0")
                            .button("E", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " E" + " 0")
                            .button("F", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " F" + " 0")
                            .button("G", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " G" + " 0")
                            .button("H", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " H" + " 0")
                            .endRow()
                            .row()
                            .button("I", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " I" + " 0")
                            .button("J", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " J" + " 0")
                            .button("K", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " K" + " 0")
                            .button("L", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " L" + " 0")
                            .button("M", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " M" + " 0")
                            .button("N", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " N" + " 0")
                            .button("O", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " O" + " 0")
                            .button("P", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " P" + " 0")
                            .endRow()
                            .row()
                            .button("Q", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " Q" + " 0")
                            .button("R", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " R" + " 0")
                            .button("S", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " S" + " 0")
                            .button("T", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " T" + " 0")
                            .button("U", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " U" + " 0")
                            .button("V", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " V" + " 0")
                            .button("W", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " W" + " 0")
                            .button("X", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " X" + " 0")
                            .endRow()
                            .row()
                            .button("Y", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " Y" + " 0")
                            .button("Z", "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " Z" + " 0")
                            .endRow()
                            .row()
                            .button("<< Back", "/"+Command.GETREQOPTION.toString().toLowerCase())
                            .endRow()
                            .buildInLineKeyboard());
        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }
}
