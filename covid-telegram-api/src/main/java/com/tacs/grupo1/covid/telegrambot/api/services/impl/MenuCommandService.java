package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.dto.UserInformation;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.ListRepository;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.UserRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class MenuCommandService implements CommandService {

    private final UserRepository userRepository;

    @Autowired
    public MenuCommandService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {

        if (update.hasCallbackQuery()) {
            UserInformation userInformation = userRepository.validateUser(update.getCallbackQuery().getMessage().getChatId());
            if(userInformation.getUserName()==null || userInformation.getUserName()=="")
                return new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setText("Cliente telegram no registrado!\n\n Ejecutar comando /start");

            return new EditMessageText()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText("Sobre que quiere operar:")
                    .setParseMode(ParseMode.MARKDOWN)
                    .setReplyMarkup(InlineKeyboardBuilder.create()
                            .row()
                            .button("Consultas", "/"+Command.GETREQOPTION.toString().toLowerCase())
                            .button("Listas", "/"+Command.GETOPLISTS.toString().toLowerCase())
                            .endRow()
                            .buildInLineKeyboard());
        }
        else if (update.hasMessage()){
            UserInformation userInformation = userRepository.validateUser(update.getMessage().getChatId());
            if(userInformation.getUserName()==null || userInformation.getUserName()=="")
                return new SendMessage().setChatId(update.getMessage().getChatId()).setText("Cliente telegram no registrado!\n\n Ejecutar comando /start");

            return InlineKeyboardBuilder.create(update.getMessage().getChatId())
                    .setText("Sobre que quiere operar:")
                    .row()
                    .button("Consultas", "/"+Command.GETREQOPTION.toString().toLowerCase())
                    .button("Listas", "/"+Command.GETOPLISTS.toString().toLowerCase())
                    .endRow()
                    .build();
        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado!");
    }
}
