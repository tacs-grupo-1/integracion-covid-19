package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.dto.CountryList;
import com.tacs.grupo1.covid.telegrambot.api.dto.UserInformation;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.ListRepository;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.UserRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import com.tacs.grupo1.covid.telegrambot.api.utils.NumericHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.io.Serializable;
import java.util.List;

@Service
public class RecuperadosCommandService implements CommandService {

    private final ListRepository listRepository;
    private final UserRepository userRepository;

    @Autowired
    public RecuperadosCommandService(ListRepository listRepository, UserRepository userRepository) {
        this.listRepository = listRepository;
        this.userRepository = userRepository;
    }
    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {
        if (update.hasMessage()){
            if(args.size() > 0 && NumericHelper.isNumeric(args.get(0))) {
                UserInformation userInformation = userRepository.validateUser(update.getMessage().getChatId());
                if(userInformation.getUserName()==null || userInformation.getUserName()=="")
                    return new SendMessage().setChatId(update.getMessage().getChatId()).setText("Cliente telegram no registrado!");

                return new SendMessage()
                        .setChatId(update.getMessage().getChatId())
                        .setText("La tabla de cual lista desea ver...")
                        .setParseMode(ParseMode.MARKDOWN)
                        .setReplyMarkup(generateKeyboardFromCountriesList(
                            listRepository.getCountriesListByTelegramId(userInformation.getTelegramId()),
                            "/" + Command.GENSTATISTICSTABLE.toString().toLowerCase(),
                            Command.RECUPERADOS.toString().toLowerCase(),
                            args.get(0),
                            "/" + Command.TABLESMENU.toString().toLowerCase()));
           }
            return new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText("Debe indicar la cantidad de días!");
        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }

    private InlineKeyboardMarkup generateKeyboardFromCountriesList(
            List<CountryList> lists, String command, String condition, String days, String backAction){

        InlineKeyboardBuilder keyboard = InlineKeyboardBuilder.create();
        boolean isEndRow=false;

        for (CountryList e: lists) {
            if(!isEndRow){
                keyboard
                        .row()
                        .button(e.getName(),
                                command + " " + e.getId() + " " + condition + " " + days);
            }
            else{
                keyboard
                        .button(e.getName(),
                                command + " " + e.getId() + " " + condition + " " + days)
                        .endRow();
            }
            isEndRow=!isEndRow;
        }

        if (isEndRow)
            keyboard.endRow();

        keyboard.row().button("<< Back",backAction).endRow();

        return keyboard.buildInLineKeyboard();
    }
}
