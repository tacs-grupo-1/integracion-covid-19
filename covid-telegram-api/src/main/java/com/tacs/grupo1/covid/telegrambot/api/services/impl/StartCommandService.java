package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.dto.UserInformation;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.UserRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class StartCommandService implements CommandService {

    private final UserRepository userRepository;

    @Autowired
    public StartCommandService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {
        if (args.size() == 0){
            if (update.hasMessage()){
                String msj = "";

                UserInformation user =userRepository.validateUser(update.getMessage().getFrom().getId());

                if (user.getUserName() != null){
                    msj = "BOT TACS GRUPO 1\n\n"
                            + "Su cuenta de telegram ya esta asociada a una cuenta en la aplicacion covid-api.\n"
                            + "Para iniciar ejecute el comando\n\n"
                            + "/menu\n";
                }
                else{
                    msj = "BOT TACS GRUPO 1 \n\n"
                            + "Para poder empezar a operar debe asociar primero la cuenta de telegram\n"
                            + "con un usuario dado de alta en la aplicación covid-api.\n"
                            + "Esta operación se realiza solo una vez y puede desasociar la cuenta en cualquier momento.\n\n"
                            + "- Asociar cuenta:\n"
                            + "Enviar el mensaje /signup seguido del usuario y password registrados en la aplicacion covid-api\n"
                            + "/signup @usuario @password\n\n"
                            + "- Desasociar cuenta:\n"
                            + "Enviar el mensaje /signout\n"
                            + "/signout\n";
                }

                // El userId ya esta registrado
                return new SendMessage()
                        .setText(msj)
                        .setChatId(update.getMessage().getChatId());

            }
            return new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText("Comando no aceptado!");
        }else{
            return new SendMessage()
                    .setText("Ingrese el comando con la cantidad correcta de argumentos")
                    .setChatId(update.getMessage().getChatId());
        }
    }
}
