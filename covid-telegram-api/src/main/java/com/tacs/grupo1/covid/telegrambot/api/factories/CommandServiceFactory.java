package com.tacs.grupo1.covid.telegrambot.api.factories;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;

@FunctionalInterface
public interface CommandServiceFactory {
    CommandService create(Command command);
}
