package com.tacs.grupo1.covid.telegrambot.api.services;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;

@FunctionalInterface
public interface CommandResolverService {
    BotApiMethod<? extends Serializable> execute(Update update);
}
