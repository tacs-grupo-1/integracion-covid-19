package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.dto.CountryList;
import com.tacs.grupo1.covid.telegrambot.api.dto.UserInformation;
import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.CountryListPlot;
import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.CountryPlot;
import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.StatisticsDay;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.ListRepository;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.StatisticsRepository;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.UserRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import com.tacs.grupo1.covid.telegrambot.api.utils.NumericHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class GenStatisticsTableCommandService implements CommandService {

    private final StatisticsRepository statisticsRepository;
    private final UserRepository userRepository;

    @Autowired
    public GenStatisticsTableCommandService(StatisticsRepository statisticsRepository, UserRepository userRepository) {
        this.statisticsRepository = statisticsRepository;
        this.userRepository = userRepository;
    }
    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {
        if (update.hasCallbackQuery()) {

            if(args.size() > 0 && NumericHelper.isNumeric(args.get(0))) {

                UserInformation userInformation = userRepository.validateUser(update.getCallbackQuery().getMessage().getChatId());
                /*
                Validar la respuesta de userInformation
                */

                String msj = genMessage(statisticsRepository.getCountryListStatisticsLastDays(Long.parseLong(args.get(0)),Long.parseLong("-"+args.get(2))),
                        args.get(1).toLowerCase(),args.get(2));

                return new SendMessage()
                        .setChatId(update.getCallbackQuery().getMessage().getChatId())
                        .setText(msj);
           }
            return new SendMessage()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setText("Debe indicar la cantidad de días!");
        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }

    public String genMessage(CountryListPlot plotData, String criteria, String days){

        Map<Date, List<String>> mapAux = new TreeMap<Date, List<String>>();
        String value="";

        for (CountryPlot cp : plotData.getCountryPlotList()) {

            String countryName = cp.getCountry().getName();

            for (StatisticsDay sd : cp.getStatisticsDayList()){

                if(criteria.equalsIgnoreCase(Command.CONFIRMADOS.toString())){
                    value = countryName +": " + sd.getConfirmed();
                }
                else if(criteria.equalsIgnoreCase(Command.RECUPERADOS.toString())){
                    value = countryName +": " + sd.getRecovered();
                }
                else if(criteria.equalsIgnoreCase(Command.MUERTOS.toString())){
                    value = countryName +": " + sd.getDeaths();
                }

                if(mapAux.containsKey(sd.getDate())){
                    mapAux.get(sd.getDate()).add(value);
                }
                else{
                    List<String> auxList = new ArrayList<String>();
                    auxList.add(value);
                    mapAux.put(sd.getDate(), auxList);
                }
            }
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String msj = "Criterio de comparación: "+ criteria + "\nEn los últimos " + days +" días\n";
        for (Map.Entry<Date, List<String>> e : mapAux.entrySet()){
            msj += "\n" + formatter.format(e.getKey()) + ":\n";
            for (String s : e.getValue()) {
                msj += "\t"+s +"\n";
            }
        }
        return msj;
    }
}
