package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.dto.Country;
import com.tacs.grupo1.covid.telegrambot.api.dto.CountryList;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.ListRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.io.Serializable;
import java.util.List;

@Service
public class GetCountriesInListCommandService implements CommandService {

    private final ListRepository listRepository;

    @Autowired
    public GetCountriesInListCommandService(ListRepository listRepository) {
        this.listRepository = listRepository;
    }

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {
        if (update.hasCallbackQuery()) {

            return new SendMessage()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setText(generateMessageTextFromCountriesList(listRepository.getCountriesListInfo(
                            1,
                            Long.parseLong(args.get(0)))));
        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }

    private String generateMessageTextFromCountriesList(CountryList countryList){
        String msj="Paises en lista: " + countryList.getName()+"\n\n";

        for (Country e: countryList.getCountries()) {
            msj+="- "+e.getName()+" ("+e.getIsoCountryCode()+")\n";
        }

        return msj;
    }
}
