package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.File;
import java.io.Serializable;
import java.util.List;

@Service
public class GetImgCommandService implements CommandService {

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {

        if (update.hasMessage()) {
            String msj = "Mensaje recibido, enviando imagen";
/*
            // Create send method
            SendDocument sendPhotoRequest = new SendDocument();
            // Set destination chat id
            sendPhotoRequest.setChatId(update.getMessage().getChatId());
            // Set the photo file as a new photo (You can also use InputStream with a method overload)
//                sendPhotoRequest.setDocument(new File("C:\\Users\\Gustavo\\Documents\\workspace-sts-4-4.6.0.RELEASE\\Html2Img\\1100x630.png"));
*/
            return new SendMessage()
                    .setText(msj)
                    .setChatId(update.getMessage().getChatId());

        } else {
            return new SendMessage()
                    .setText("Ingrese el comando con la cantidad correcta de argumentos")
                    .setChatId(update.getMessage().getChatId());
        }

    }
}