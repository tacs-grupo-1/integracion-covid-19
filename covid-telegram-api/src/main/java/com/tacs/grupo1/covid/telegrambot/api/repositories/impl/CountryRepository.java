package com.tacs.grupo1.covid.telegrambot.api.repositories.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tacs.grupo1.covid.telegrambot.api.dto.Countries;
import com.tacs.grupo1.covid.telegrambot.api.dto.CountriesPage;
import com.tacs.grupo1.covid.telegrambot.api.dto.Country;
import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.CountryListPlot;
import com.tacs.grupo1.covid.telegrambot.api.exceptions.RestRepositoryException;
import com.tacs.grupo1.covid.telegrambot.api.repositories.AbstractRestRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;

import java.util.Arrays;
import java.util.List;

import static java.util.Objects.nonNull;

@Slf4j
@Repository
public class CountryRepository extends AbstractRestRepository {

    private LoginRepository loginRepository;
    @Value("${apiaddr}")
    private String apiaddr;

    @Autowired
    public CountryRepository(RestTemplateBuilder restBuilder, LoginRepository loginRepository) {
        super(restBuilder, loginRepository);
    }
/*
    public Countries getCountries(long userId) {
        try {
            String uri = buildGetCountriesUrl(userId);
            ResponseEntity<Countries> response = get(uri, Countries.class);

            return response.getBody();

        } catch (RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException();
        }
    }
*/
    public List<Country> getCountries(long userId) {
        try {
            String uri = buildGetCountriesUrl(userId);
            ResponseEntity<String> response = get(uri, String.class);
            String body = response.getBody();
            if (nonNull(body)) {
                Country[] data = new ObjectMapper().readValue(body, Country[].class);
                return Arrays.asList(data);
            }
        } catch (JsonProcessingException | RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while getting Countries");
        }
        return null;
    }

    public CountriesPage getCountriesPage(String prefix, int pageNum) {
        try {
            String uri = buildGetCountriesPageUrl(prefix,pageNum);
            ResponseEntity<CountriesPage> response = get(uri, CountriesPage.class);
            return response.getBody();
        } catch (RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while getting countries page");
        }
    }

    private String buildGetCountriesUrl(long userId) {
        return getUriBuilder(getBaseUri() + "/countries")
                .queryParam("telegram_user_id", userId).toUriString();
    }

    private String buildGetCountriesPageUrl(String prefix, int pageNum) {
        return getUriBuilder(getBaseUri() + "/countries-page")
                .queryParam("prefix", prefix)
                .queryParam("pageNum", pageNum)
                .toUriString();
    }

    private String getBaseUri() {
        return this.apiaddr;
    }

}
