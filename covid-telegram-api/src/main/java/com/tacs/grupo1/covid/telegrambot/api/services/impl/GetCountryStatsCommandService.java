package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.CountryStatistics;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.StatisticsRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class GetCountryStatsCommandService implements CommandService {

    private final StatisticsRepository statisticsRepository;

    @Autowired
    public GetCountryStatsCommandService(StatisticsRepository statisticsRepository) {
        this.statisticsRepository = statisticsRepository;
    }

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {
        if (update.hasCallbackQuery()) {

            CountryStatistics countryStatistics = statisticsRepository
                    .getCountryStatistics(Long.parseLong(args.get(0)), 1);

            String msj = countryStatistics.getCountry().getName() + "\n"
                    + "confirmed: " + countryStatistics.getStatistics().getConfirmed() + "\n"
                    + "recovered: " + countryStatistics.getStatistics().getRecovered() + "\n"
                    + "death: " + countryStatistics.getStatistics().getDeaths();

            return new SendMessage()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setText(msj);
//                    .setText("Informacion del pais " + args.get(0))
        }
        else if (update.hasMessage()){
            return new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText("Comando no aceptado por mensaje!");
        }

        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }
}
