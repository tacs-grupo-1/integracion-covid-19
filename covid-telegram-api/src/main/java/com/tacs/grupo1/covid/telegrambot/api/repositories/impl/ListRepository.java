package com.tacs.grupo1.covid.telegrambot.api.repositories.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tacs.grupo1.covid.telegrambot.api.dto.*;
import com.tacs.grupo1.covid.telegrambot.api.exceptions.RestRepositoryException;
import com.tacs.grupo1.covid.telegrambot.api.repositories.AbstractRestRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;

import java.util.Arrays;
import java.util.List;

import static java.util.Objects.nonNull;

@Slf4j
@Repository
public class ListRepository extends AbstractRestRepository {

    private LoginRepository loginRepository;
    @Value("${apiaddr}")
    private String apiaddr;

    @Autowired
    public ListRepository(RestTemplateBuilder restBuilder, LoginRepository loginRepository) {
        super(restBuilder, loginRepository);
    }

    /*
    public List<CountryList> getCountriesList(long userId) {
        try {
            String uri = buildGetCountriesListUrl(userId);
            ResponseEntity<String> response = get(uri, String.class);
            String body = response.getBody();
            if (nonNull(body)) {
                CountryList[] data = new ObjectMapper().readValue(body, CountryList[].class);
                return Arrays.asList(data);
            }
        } catch (JsonProcessingException | RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException();
        }
        return null;
    }*/

    public CountryList addCountry (long listId, long countryId) {
        try {
            String uri = builAddCountryUrl(listId, countryId);
            ResponseEntity<CountryList> response = post(uri, CountryList.class,null);
            return response.getBody();
        } catch (RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while adding Country");
        }
    }

    //  USANDO TOKEN TELEGRAM
    public List<CountryList> getCountriesListByTelegramId(long telegramId) {
        try {
            String uri = buildGetCountriesListByTelegramIdUrl();
            ResponseEntity<String> response = getv2(uri, String.class, telegramId);
            String body = response.getBody();
            if (nonNull(body)) {
                CountryList[] data = new ObjectMapper().readValue(body, CountryList[].class);
                return Arrays.asList(data);
            }
        } catch (JsonProcessingException | RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while getting Country Lists by Telegram ID");
        }
        return null;
    }

    public List<CountryList> getCountriesListByUserId(long userId) {
        try {
            String uri = buildGetCountriesListByUserIdUrl(userId);
            ResponseEntity<String> response = get(uri, String.class);
            String body = response.getBody();
            if (nonNull(body)) {
                CountryList[] data = new ObjectMapper().readValue(body, CountryList[].class);
                return Arrays.asList(data);
            }
        } catch (JsonProcessingException | RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while getting Country Lists by User ID");
        }
        return null;
    }

    public CountryList getCountriesListInfo(long userId, long listId) {
        try {
            String uri = buildGetCountriesLisInfotUrl(listId, userId);
            ResponseEntity<CountryList> response = get(uri, CountryList.class);
            return response.getBody();
        } catch (RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while getting Country List Info");
        }
    }

    private String buildGetCountriesListUrl(long userId) {
        return getUriBuilder(getBaseUri() + "/country-lists")
                .queryParam("telegram_user_id", userId).toUriString();
    }

    private String builAddCountryUrl(long listId, long countryId) {
        return getUriBuilder(getBaseUri() + "/country-lists/"+listId+"/countries/"+countryId)
                .queryParam("telegram_user_id", 1).toUriString();
    }

    private String buildGetCountriesListByTelegramIdUrl() {
        return getUriBuilder(getBaseUri() +"/users/country-lists").toUriString();
    }

    private String buildGetCountriesListByUserIdUrl(long userId) {
        return getUriBuilder(getBaseUri() +"/users/country-lists").toUriString();
    }

    private String buildGetCountriesLisInfotUrl(long listId, long userId) {
        return getUriBuilder(getBaseUri() + "/country-lists/" +listId).toUriString();
    }

    private String getBaseUri() {
        return apiaddr;
    }

}
